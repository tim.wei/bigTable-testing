package main

import (
	//"bytes"
	//"encoding/gob"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"reflect"
	//"time"
	//"os"
	//"os/signal"
	//"syscall"

	"cloud.google.com/go/bigtable"
	"github.com/gorilla/mux"
	"golang.org/x/net/context"
	"google.golang.org/appengine"
)

const (
	tableName        = "person"
	columnFamilyName = "info"
)

type msg struct {
	Message string
}

type personInfo struct {
	Name string //name
	Arg  string //arge
	Gps  string //GPS
	Time string //Time
}

var (
	ctx         context.Context
	adminClient *bigtable.AdminClient
	client      *bigtable.Client
	tbl         *bigtable.Table
)

func sliceContains(list []string, target string) bool { //compare list[] and target
	for _, s := range list {
		if s == target {
			return true
		}
	}
	return false
} //end sliceContains

func initBigTable() {
	project := "dark-form-199907"
	instance := "bigtable"
	ctx = context.Background() //timFlag@ ??
	var err error

	adminClient, err = bigtable.NewAdminClient(ctx, project, instance) //create manage table link
	if err != nil {
		log.Fatalf("Could not create admin client: %v", err)
	}

	tables, err := adminClient.Tables(ctx) //get list all table
	if err != nil {
		log.Fatalf("Could not fetch table list: %v", err)
	}

	//if not exist, create Table
	if !sliceContains(tables, tableName) {
		log.Printf("Creating table %s", tableName)
		if err := adminClient.CreateTable(ctx, tableName); err != nil {
			log.Fatalf("Could not create table %s: %v", tableName, err)
		}
	}

	tblInfo, err := adminClient.TableInfo(ctx, tableName)
	if err != nil {
		log.Fatalf("Could not read info for table %s: %v", tableName, err)
	}

	//if not exist, create columnFamily
	if !sliceContains(tblInfo.Families, columnFamilyName) {
		if err := adminClient.CreateColumnFamily(ctx, tableName, columnFamilyName); err != nil {
			log.Fatalf("Could not create column family %s: %v", columnFamilyName, err)
		}
	}

	// Set up operations client.
	client, err = bigtable.NewClient(ctx, project, instance)
	if err != nil {
		log.Fatalf("Could not create data operations client: %v", err)
	}

	tbl = client.Open(tableName)

} //end initBigTable()

func respondWithJson(w http.ResponseWriter, code int, payload interface{}) { //output JSON
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
} //end responWithJson()

func respondWithError(w http.ResponseWriter, code int, mesg string) {
	respondWithJson(w, code, msg{mesg})
} //end respondWithError()

func itf2String(in interface{}) string {
	return in.(string)
} //end itf2String()

func listData(w http.ResponseWriter, r *http.Request) {
	log.Println("List Data")
	//log.Println("List Table")
	//tables, err := adminClient.Tables(ctx)
	//if err != nil {
	//	respondWithError(w, http.StatusInternalServerError, "Could not fetch table list")
	//	log.Fatalf("Could not fetch table list: %v", err)
	//}

	//log.Println("List ColumFamilyName")
	//tblInfo, err := adminClient.TableInfo(ctx, tableName)
	//if err != nil {
	//	log.Fatalf("Could not read info for table %s: %v", tableName, err)
	//} else {
	//	for _, v := range tblInfo.Families {
	//		log.Printf(v)
	//	}
	//}

	p := personInfo{}
	e := reflect.ValueOf(&p).Elem()
	for i := 0; i < e.NumField(); i++ {
		columnName := e.Type().Field(i).Name
		rowKey := fmt.Sprintf("%s", columnFamilyName+columnName)
		row, err := tbl.ReadRow(ctx, rowKey, bigtable.RowFilter(bigtable.ColumnFilter(columnName)))
		if err != nil || len(row) == 0 {
			log.Printf("Could not read row with key %s: %v", rowKey, err)
			respondWithError(w, http.StatusBadRequest, "Could not find item")
			return
		}
		log.Printf("\t%s = %s\n", rowKey, string(row[columnFamilyName][0].Value))
		e.Field(i).SetString(string(row[columnFamilyName][0].Value))
	} //end for(e.NumField())
	respondWithJson(w, http.StatusOK, p)
} //end listData()

func getData(w http.ResponseWriter, r *http.Request) {
	log.Println("Get")
	//get params
	params := mux.Vars(r)
    columnName := params["ask"]
	rowKey := fmt.Sprintf("%s", columnFamilyName+columnName)
	row, err := tbl.ReadRow(ctx, rowKey, bigtable.RowFilter(bigtable.ColumnFilter(columnName)))
	if err != nil || len(row) == 0 {
		log.Printf("Could not read row with key %s: %v", rowKey, err)
		respondWithError(w, http.StatusBadRequest, "Could not find item")
		return
	}
    out := map[string]string {
        columnName:string(row[columnFamilyName][0].Value),
    }
	respondWithJson(w, http.StatusOK, out)
} //end getData

func createData(w http.ResponseWriter, r *http.Request) { //POST
	log.Println("Create")

	p := personInfo{}
	if err := json.NewDecoder(r.Body).Decode(&p); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		log.Println("decode Error")
		return
	}

	log.Printf("name: %s\n", p.Name)
	log.Printf("arg: %s\n", p.Arg)
	log.Printf("gps: %s\n", p.Gps)
	log.Printf("time: %s\n", p.Time)

	muts := bigtable.NewMutation()
	rowKey := ""
	//write struct via reflect
	e := reflect.ValueOf(&p).Elem()
	for i := 0; i < e.NumField(); i++ {
		columnName := e.Type().Field(i).Name
		muts.Set(columnFamilyName, columnName, bigtable.Now(), []byte(itf2String(e.Field(i).Interface())))
		rowKey = fmt.Sprintf("%s", columnFamilyName+columnName)
		log.Println("rowKey:", rowKey)

		err := tbl.Apply(ctx, rowKey, muts)
		if err != nil {
			log.Printf("Could not apply row mutation: %v", err)
			respondWithError(w, http.StatusBadRequest, "Could not apply row mutation")
			return
		}
	} //end for(e.NumField())
	respondWithJson(w, http.StatusOK, p)
} //end createData()

func updateData(w http.ResponseWriter, r *http.Request) { //PUT
	//same Creat?
	log.Println("Update")

	p := personInfo{}
	if err := json.NewDecoder(r.Body).Decode(&p); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		log.Println("decode Error")
		return
	}

	muts := bigtable.NewMutation()
	rowKey := ""
	//write struct via reflect
	e := reflect.ValueOf(&p).Elem()
	for i := 0; i < e.NumField(); i++ {
		columnName := e.Type().Field(i).Name
		muts.Set(columnFamilyName, columnName, bigtable.Now(), []byte(itf2String(e.Field(i).Interface())))
		rowKey = fmt.Sprintf("%s", columnFamilyName+columnName)
		log.Println("rowKey:", rowKey)

		err := tbl.Apply(ctx, rowKey, muts)
		if err != nil {
			log.Printf("Could not apply row mutation: %v", err)
			respondWithError(w, http.StatusBadRequest, "Could not apply row mutation")
			return
		}
	} //end for(e.NumField())
	respondWithJson(w, http.StatusOK, p)
} //end updateData()

func deleteData(w http.ResponseWriter, r *http.Request) {
	log.Println("Delete")

	p := personInfo{}
	if err := json.NewDecoder(r.Body).Decode(&p); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		log.Println("decode Error")
		return
	}

	muts := bigtable.NewMutation()
	e := reflect.ValueOf(&p).Elem()
	for i := 0; i < e.NumField(); i++ {
		columnName := e.Type().Field(i).Name
		rowKey := fmt.Sprintf("%s", columnFamilyName+columnName)
		muts.DeleteCellsInColumn(columnFamilyName, columnName)
		err := tbl.Apply(ctx, rowKey, muts)
		if err != nil {
			log.Printf("Could not apply row mutation: %v", err)
			respondWithError(w, http.StatusBadRequest, "Could not apply row mutation")
			return
		}
	} //end for(e.NumField())
	respondWithJson(w, http.StatusOK, p)
} //end deleteData()

func main() {
	initBigTable()
	router := mux.NewRouter()
	router.HandleFunc("/", listData).Methods("GET")
	router.HandleFunc("/{ask}", getData).Methods("GET")
	router.HandleFunc("/", createData).Methods("POST")
	router.HandleFunc("/", updateData).Methods("PUT")
	router.HandleFunc("/", deleteData).Methods("DELETE")
	log.Fatal(http.ListenAndServe(":8080", router))
	appengine.Main()
}
